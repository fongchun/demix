import wrapdemix
import wraptitan
import wraptheta
import wrapclonehd

catalog = dict()
catalog['demix'] = wrapdemix.DemixTool
catalog['titan'] = wraptitan.TitanTool
catalog['theta'] = wraptheta.ThetaTool
catalog['clonehd'] = wrapclonehd.CloneHDTool

